FROM python:3.9-bullseye

ENV DEBIAN_FRONTEND=noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN=true

RUN apt-get -y update && apt-get -y install build-essential sdcc

RUN pip3 install pillow==2.2.2 black==22.1.0

RUN wget http://www.julien-nevo.com/arkostracker/release/linux64/Arkos%20Tracker%202%20Linux64.zip \
 && unzip -p Arkos\ Tracker\ 2\ Linux64.zip 'Arkos Tracker 2/tools/Disark' > /usr/local/bin/Disark \
 && chmod 755 /usr/local/bin/Disark && rm Arkos\ Tracker\ 2\ Linux64.zip
